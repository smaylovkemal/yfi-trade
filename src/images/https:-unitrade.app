<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
    <link rel="manifest" href="site.webmanifest">

    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/all.min.css"></link>
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="css/styles.css?v=1.0.2"></link>

    <title>UniTrade.app - DeFi Trading (ETH & ERC-20). Schedule Buys/Sells against Uniswap liquidity pools.</title>
    <meta name="description" content="UniTrade is a completely decentralized trading platform in the DeFi space that is built on top of UniSwap liquidity pools and requires no KYC, no accounts, and has no trading limits.">
    <meta name=”robots” content="index, follow">
</head>
<body>
    <div class="texture-bg hero-wrapper">
        <header class="container-fluid container-lg">
            <div class="row">
                <div class="col-md-4 col-lg-5">
                    <a href="/" class="logo" data-aos="fade-down"  data-aos-delay="600">
                        <img src="images/unitrade_logo_transparent.png" alt="UniTrade Logo" />
                    </a>

                    <ul class="social-links gray">
                        <li data-aos="fade-up"  data-aos-delay="200"><a href="https://t.me/UniTradeApp" target="_blank"><i class="fab fa-telegram-plane"></i></a></li>
                        <li data-aos="fade-up"  data-aos-delay="300"><a href="https://twitter.com/UniTradeApp" target="_blank"><i class="fab fa-twitter"></i></a></li>
                        <li data-aos="fade-up"  data-aos-delay="400"><a href="https://discord.gg/tvH5bMm" target="_blank"><i class="fab fa-discord"></i></a></li>
                    </ul>
                </div>
                <div class="col-md-8 col-lg-7">
                    <ul class="menu">
                        <li><a href="#unitrade-platform">About</a></li>
                        <li><a href="#unitrade-benefits">Features</a></li>
                        <li><a href="#what-is-unitrade-token-for">Token</a></li>
                        <li><a href="#unitrade-roadmap">Roadmap</a></li>
                        <li><a href="#unitrade-faq">FAQ</a></li>
                        <li><a class="btn-circle" href="mailto:contact@unitrade.app">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </header>

        <section id="hero" class="container section full-height">
            <h1 data-aos="fade-up" data-aos-delay="200">DeFi Trading Platform built from <span>Uniswap</span></h1>
            <h2 data-aos="fade-up" data-aos-delay="400">Schedule Buys / Sells, No KYC or Limits, Professional Trading Tools, Completely Decentralized.</h2>
            <div class="button-holder">
                <a data-aos="fade-up" data-aos-delay="1000" href="https://uniswap.info/token/0x6f87d756daf0503d08eb8993686c7fc01dc44fb1" class="button arrow" target="_blank">Buy TRADE</a>
                <a data-aos="fade-up" data-aos-delay="1200" href="./pdfs/UniTrade-Whitepaper.pdf" class="button paper" target="_blank">Whitepaper</a>
            </div>

            <p>Let us <b>show you the new</b> way to trade on Uniswap <i class="fas fa-chevron-down"></i></p>
        </section>

        <img class="unitrade-preview-dashboard" data-aos="zoom-in-up" data-aos-offset="240" data-aos-duration="900" src="images/UniTrade_Platform_bg.png" alt="UniTrade live dashboard platform" />

        <div class="bg-img-1" style="background-image: url('images/header_dots_circle.png')"></div>
        <div class="bg-img-2" style="background-image: url('images/header_dots_square.png')"></div>
    </div>

    <section id="unitrade-platform" class="section white-bg full-height" style="position: relative; z-index: 5;">
        <div class="container">
            <div class="row" style="overflow-x: hidden; overflow-y: hidden;">
                <div class="col-md-6">
                    <h1><span>UniTrade</span> - a DeFi Exchange Platform</h1>
                    <p>UniTrade is a completely decentralized trading platform in the DeFi space that is built on top of UniSwap liquidity pools and requires no KYC, no accounts, and has no trading limits.</p>
                    
                    <ul>
                        <li data-aos="fade-left">UniTrade will tap into the ~$200M a day trading volume on UniSwap (a liquidity pool provider) which currently holds a 40% marketshare among other DEX's.</li>
                        <li data-aos="fade-left" data-aos-offset="200">DEX's (Decentralized Exchanges) are rapidly growing in popularity. UniSwap's userbase has exponentially grown, recently adding 27k new unique users in a single month. We provide these users with a sleek interface and powerful trading tools they are asking for.</li>
                    </ul>
                </div>

                <div class="col-md-6 platform-image" style="overflow: hidden">
                    <div class="unitrade-platform-img-wrapper">
                        <img data-aos="zoom-in-up" data-aos-offset="235" data-aos-duration="900" class="img-preview-1" src="images/UniTrade_Trade_preview.png" alt="Unitrade trading view preview" />
                        <img data-aos="zoom-in-up" data-aos-offset="125" data-aos-duration="700" class="img-preview-2" src="images/UniTrade_preview_small.png" alt="Unitrade trading view preview" />
                    </div>
                </div>
            </div>
        </div>

        <div class="bg-img-1" style="background-image: url('images/dots_square.png')"></div>
    </section>

    <div class="texture-bg">
    <section id="unitrade-benefits" class="section">
        <div class="container">
            <div class="row" style="overflow-x: hidden">
                <div class="col-md-4 col-sm-6">
                    <h1>Why does this <span>matter</span></h1>
                </div>

                <div class="col-md-4 col-sm-6" data-aos="fade-left">
                    <div class="unitrade-benefits-box">
                        <i class="fas fa-redo-alt"></i>
                        <h4>Automate Your Trades</h4>
                        <p>You will now be able to place sell or buy orders at target prices on Uniswap. Through the UniTrade platform you will be able to execute a buy order if a token's price reaches $0.50 for example or sell if it drops below $0.25.</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6" data-aos="fade-left">
                    <div class="unitrade-benefits-box">
                        <i class="far fa-calendar-check"></i>
                        <h4>Schedule Buys & Sells</h4>
                        <p>With the UniTrade platform you can seamlessly schedule recurring buy orders or sell orders over a duration of time with specific parameters such as the price range of a token on UniSwap.</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="unitrade-benefits-box">
                        <i class="fas fa-tint"></i>
                        <h4>Liquidity Management</h4>
                        <p>Maximize earnings by setting breakpoints that will add or remove your liquidity pairs from Uniswap pools if the price falls below or increases past a set amount.</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6" data-aos="fade-left">
                    <div class="unitrade-benefits-box">
                        <i class="fas fa-chart-bar"></i>
                        <h4>Charts and Analytics</h4>
                        <p>View detailed charts of all the live trade action happening on Uniswap that you would expect to see on professional trading software. This includes buys, sells, liquidity add/remove, volume, and much more.</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6" data-aos="fade-left">
                    <div class="unitrade-benefits-box">
                        <i class="fas fa-book"></i>
                        <h4>Live Order Book</h4>
                        <p>Preview all of the buys and sells that are happening given a specific token pair on the UniTrade platform.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="bg-img-special">
        <div class="bg-img-1" style="background-image: url('images/dots_triangle.png')"></div>
    </div>

    <section id="unitrade-quote" class="section" data-aos="fade-up-right" data-aos-offset="120" data-aos-duration="700">
        <div class="container">
            <div class="unitrade-logo">
                <img src="">
                <i class="fas fa-quote-left"></i>
            </div>
            <p class="quote">
                "Uniswap is an amazing platform but it's so new and there is so many times I wish I could setup <span>buy orders or sell orders at a specific prices for tokens.</span> There just wasn't anything out there to simply automate that.<br><span>That's when the UniTrade idea formed.</span>"
            </p>
            <p class="quote-author"><a href="https://twitter.com/reborn_1002" target="_blank"><i class="fab fa-twitter"></i> @Reborn1002</a>, Founder &amp; Developer</p>
        </div>
    </section>
    </div>

    <section id="what-is-unitrade-token-for" class="section white-bg">
        <div class="container">
            <h1>What is the UniTrade (TRADE) <span>token</span> for?</h1>

            <div class="unitrade-token-for-mobile"  style="overflow-x: hidden">
                
                <div class="block" data-aos="fade-left">
                    <div class="emoji" style="background-color: #dff0e1;" data-aos="zoom-in-up" data-aos-offset="200">&#128176;</div>
                    <p>ETH from the fees on the platform are given as rewards to TRADE token holders.</p>
                </div>

                <div class="block" data-aos="fade-left">
                    <div class="emoji" style="background-color: #FFF4EA;" data-aos="zoom-in-up" data-aos-offset="200">&#129304;</div>
                    <p>Token holders will receive discounts on the trading platform.</p>
                </div>

                <div class="block" data-aos="fade-left">
                    <div class="emoji" style="background-color: #FFEAEA;" data-aos="zoom-in-up" data-aos-offset="200">&#128293;</div>
                    <p>TRADE tokens are purchased with ETH from the collected fees and burned by the contract.</p>
                </div>

                <div class="steps">
                    <div class="block" data-aos="fade-left">
                        <div class="emoji" style="background-color: #e9f1ff;" data-aos="zoom-in-up" data-aos-offset="200"><i class="fas fa-coins"></i></div>
                        <div>
                            <h4>Small fee is collected</h4>
                            <p>To make trades on UniTrade a small fee is collected in ETH.</p>
                        </div>
                    </div>

                    <div class="block" data-aos="fade-left">
                        <div class="emoji" style="background-color: #e9f1ff;" data-aos="zoom-in-up" data-aos-offset="200"><i class="fas fa-keyboard"></i></div>
                        <div>
                            <h4>Development fund</h4>
                            <p>10% of the fees are stored for future development.</p>
                        </div>
                    </div>

                    <div class="block" data-aos="fade-left">
                        <div class="emoji" style="background-color: #e9f1ff;" data-aos="zoom-in-up" data-aos-offset="200"><i class="fas fa-money-bill-wave-alt"></i></div>
                        <div>
                            <h4>TRADE buy back</h4>
                            <p>60% of fees buy UniTrade directly from the TRADE/ETH UniSwap liquidity pool and burns them.</p>
                        </div>
                    </div>

                    <div class="block" data-aos="fade-left">
                        <div class="emoji" style="background-color: #e9f1ff;" data-aos="zoom-in-up" data-aos-offset="200"><i class="fas fa-users"></i></div>
                        <div>
                            <h4>TRADE Rewards</h4>
                            <p>The remaining 30% of the fees, in ETH, are awarded to TRADE holders daily.</p>
                        </div>
                    </div>

                </div>
                <hr />
            </div>

            <div class="unitrade-token-for">

                <div class="token-for-wrapper">

                    <div class="token-for-item">
                        <span>1</span>
                        <div class="icon-box">
                            <i class="fas fa-coins"></i>
                        </div>
                        <h4>Small fee is collected</h4>
                        <p>To make trades on UniTrade a small fee is collected in ETH.</p>
                    </div>

                    <div class="token-for-item">
                        <span>2</span>
                        <div class="icon-box">
                            <i class="fas fa-keyboard"></i>
                        </div>
                        <h4>Development fund</h4>
                            <p>10% of the fees are stored for future development.</p>
                    </div>

                    <div class="token-for-item">
                        <span>3</span>
                        <div class="icon-box">
                            <i class="fas fa-money-bill-wave-alt"></i>
                        </div>
                        <h4>TRADE buy back</h4>
                        <p>60% of fees buy UniTrade directly from the TRADE/ETH UniSwap liquidity pool and burns them.</p>
                    </div>

                    <div class="token-for-item">
                        <span>4</span>
                        <div class="icon-box">
                            <i class="fas fa-users"></i>
                        </div>
                        <h4>TRADE Rewards</h4>
                        <p>The remaining 30% of the fees, in ETH, are awarded to TRADE holders daily.</p>
                    </div>

                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="block" data-aos="fade-left">
                            <div class="emoji" style="background-color: #dff0e1;" data-aos="zoom-in-up" data-aos-offset="200" data-aos-delay="100">&#128176;</div>
                            <p>ETH from the fees on the platform are given as rewards to TRADE token holders.</p>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="block" data-aos="fade-left">
                            <div class="emoji" style="background-color: #FFF4EA;" data-aos="zoom-in-up" data-aos-offset="225">&#129304;</div>
                            <p>Token holders will receive discounts on the trading platform.</p>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="block" data-aos="fade-left">
                            <div class="emoji" style="background-color: #FFEAEA;" data-aos="zoom-in-up" data-aos-offset="200" data-aos-delay="275">&#128293;</div>
                            <p>TRADE tokens purchased with ETH from fees on UniTrade are burned by the contract.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="unitrade-token-information" class="section white-bg">

        <div class="container">
            <div class="row token-info-wrapper">
                <div class="col-md-6 order-md-2 col-lg-8">
                    <div class="unitrade-token-header">
                        <img src="images/unitrade_1024.png" alt="UniTrade token information" data-aos="fade-right"/>
                        <h3 data-aos="fade-left">UniTrade</h3>
                    </div>

                    <h1><span>Token</span> Information</h1>
                    <p>Total Supply: <span>50m</span> | Circulating Supply: <span>~22m</span></p>

                    <div class="unitrade-token-breakdown" data-aos="fade-up">

                        <table data-aos="fade-up" data-aos-offset="200">
                            <tr>
                                <td class="icon">
                                    <div class="red"></div>
                                </td>
                                <td class="text">
                                    <h4>OTC Presale</h4>
                                    <p>Now in circulation.</p>
                                </td>
                                <td class="amount">40% (20m)</td>
                            </tr>

                            <tr>
                                <td class="icon">
                                    <div class="tan"></div>
                                </td>
                                <td class="text">
                                    <h4>Liquidity &amp; Development</h4>
                                    <p>Liquidity is locked. The rest is vested over a year.</p>
                                </td>
                                <td class="amount">40% (20m)</td>
                            </tr>

                            <tr>
                                <td class="icon">
                                    <div class="lighter-red"></div>
                                </td>
                                <td class="text">
                                    <h4>Reserve &amp; Team</h4>
                                    <p>Locked, and vested over a year.</p>
                                </td>
                                <td class="amount">10% (5m)</td>
                            </tr>

                            <tr>
                                <td class="icon">
                                    <div class="pink"></div>
                                </td>
                                <td class="text">
                                    <h4>Audits, Marketing, Partnerships</h4>
                                    <p>Locked, and vested over a year.</p>
                                </td>
                                <td class="amount">10% (5m)</td>
                            </tr>
                        </table>

                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="total-supply" data-aos="fade-up">
                        <img src="images/unitrade_token_supply.png" alt="UniTrade total supply - 50 million"  data-aos="fade-up" data-aos-offset="100" data-aos-delay="100"/>
                    </div>

                    <div class="unitrade-contract-link" data-aos="fade-up">
                        <p data-aos="fade-up" data-aos-offset="170" data-aos-delay="250">
                            <span>Contract Address</span>
                            <a class="purple" href="https://etherscan.io/token/0x6F87D756DAf0503d08Eb8993686c7Fc01Dc44fB1" target="_blank">0x6F87D756DAf0503d08Eb8993686c7Fc01Dc44fB1 <i class="fas fa-external-link-alt"></i></a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-lg-5">
                    <div class="unitrade-token-info">
                        <h2>Reserve &amp; Team<span>10%</span><div class="lighter-red"></div></h2>
                        <p>These tokens will be given to the core developers and team members under a timed release schedule. Additionally a set amount of tokens will be placed in reserve for future developments.</p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-7">
                    <div class="unitrade-token-info">
                        <h2>Audits, Marketing, Partnerships<span>10%</span><div class="lighter-red"></div></h2>
                        <p>To ensure the trading platform operates at the highest level possible a set amount of tokens will be used to secure professional third party security audits. To increase exposure and use of the platform additional funds will be used for marketing, partnerships, and promotions.</p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-5">
                    <div class="unitrade-token-info">
                        <h2>OTC Presale<span>40%</span><div class="red"></div></h2>
                        <p>The presale amount is the amount of tokens that are sold privately and publicity by token holders.</p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-7">
                    <div class="unitrade-token-info">
                        <h2>Liquidity &amp; Development<span>40%</span><div class="tan"></div></h2>
                        <p>A portion of the tokens will be added to UniSwap's liquidity ETH/TRADE pair to help stabilize the price and provide liquidity for traders. The additional amount will be locked in a time released contract to be used for liquidity and further development in the future.</p>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section id="unitrade-roadmap" class="section" style="overflow-x: hidden;">
        <div class="container-md container-fluid">
            <h1>Roadmap</h1>
            <p>The future development plans for UniTrade</p>  

            <div class="roadmap-wrapper">
                <ul class="roadmap mobile">
                    <li class="complete">
                        <div class="marker" data-aos="fade-right"></div>
                        <div class="text" data-aos="fade-left" data-aos-offset="200">
                            <h1>Early August</h1>
                            <p>UniTrade backend prototype development, initial research and testing.</p>
                        </div>
                    </li>

                    <li class="complete">
                        <div class="marker" data-aos="fade-right"></div>
                        <div class="text" data-aos="fade-left" data-aos-offset="200">
                            <h1>August</h1>
                            <p>UniTrade airdrop and Uniswap exchange listing.</p>
                        </div>
                    </li>

                    <li class="complete">
                        <div class="marker" data-aos="fade-right"></div>
                        <div class="text" data-aos="fade-left" data-aos-offset="200">
                            <h1>August 14th</h1>
                            <p>UniTrade.app website launch.</p>
                        </div>
                    </li>

                    <li class="complete">
                        <div class="marker" data-aos="fade-right"></div>
                        <div class="text"data-aos="fade-left" data-aos-offset="200">
                            <h1>August 20th</h1>
                            <p>Tech & Business Whitepaper.</p>
                        </div>
                    </li>

                    <li class="complete">
                        <div class="marker" data-aos="fade-right"></div>
                        <div class="text" data-aos="fade-left" data-aos-offset="200">
                            <h1>August 22nd</h1>
                            <p>UniTrade platform UI designs finalized and front-end development begins.</p>
                        </div>
                    </li>

                    <li class="">
                        <div class="marker" data-aos="fade-right"></div>
                        <div class="text" data-aos="fade-left" data-aos-offset="200">
                            <h1>September 11th</h1>
                            <p>UniTrade.app development beta launch.</p>
                        </div>
                    </li>

                    <li class="">
                        <div class="marker" data-aos="fade-right"></div>
                        <div class="text" data-aos="fade-left" data-aos-offset="200">
                            <h1>September 14th</h1>
                            <p>Third party security audit from reputable firm.</p>
                        </div>
                    </li>

                    <li class="">
                        <div class="marker" data-aos="fade-right"></div>
                        <div class="text" data-aos="fade-left" data-aos-offset="200">
                            <h1>September 30th</h1>
                            <p>UniTrade official platform launch!</p>
                        </div>
                    </li>
                </ul>

                <div class="timeline"></div>
            </div>
            
        </div>
    </section>

    <section id="unitrade-faq" class="section">
        <div class="container">
            <hr>

            <h1>Frequently asked <span>questions</span></h1>

            <ul>
                <li>
                    <h4><a class="faq-question" href="javascript:;">What is UniTrade?<i class="fas fa-plus"></i></a></h4>
                    <div class="faq-answer-content">
                        <p>UniTrade is a completely decentralized trading platform in the DeFi space that is built on top of UniSwap liquidity pools and requires no KYC, no accounts, and has no trading limits.</p>
                    </div>
                </li>

                <li class="open">
                    <h4><a class="faq-question" href="javascript:;">Do I need an account / KYC to use UniTrade? <i class="fas fa-plus"></i></a></h4>
                    <div class="faq-answer-content">
                        <p>No, to use UniTrade you only need a web3 compatible wallet e.g. MetaMask.</p>
                    </div>
                </li>

                <li>
                    <h4><a class="faq-question" href="javascript:;">Do I need TRADE token to use UniTrade?<i class="fas fa-plus"></i></a></h4>
                    <div class="faq-answer-content">
                        <p>You don't need TRADE tokens to use UniTrade, however users that do have these tokens receive fee discounts on the trading platform.</p>
                    </div>
                </li>
            </ul>
        </div>
    </section>

    <section id="unitrade-community" class="section">
        <div class="container">
            <img src="images/unitrade_logo.jpg" alt="UniTrade Logo" />
            <h1>Join our <span>Community</span></h1>
            <p>Learn more about the project, interact with the team, and take a part in shaping the future of UniTrade.</p>

            <div class="row">
                <div class="col-md-6" data-aos="fade-up" data-aos-delay="100">
                    <a href="https://t.me/UniTradeApp" target="_blank"><i class="fab fa-telegram-plane"></i> Telegram</a>
                </div>
                <div class="col-md-6" data-aos="fade-up" data-aos-delay="150" data-aos-offset="120">
                    <a href="https://discord.gg/tvH5bMm" target="_blank"><i class="fab fa-discord"></i> Discord</a>
                </div>
                <div class="col-md-6" data-aos="fade-up" data-aos-delay="175" data-aos-offset="150">
                    <a href="https://twitter.com/UniTradeApp" target="_blank"><i class="fab fa-twitter"></i> Twitter</a>
                </div>
                <div class="col-md-6" data-aos="fade-up" data-aos-delay="200" data-aos-offset="200">
                    <a href="#" target="_blank"><i class="fab fa-reddit-alien"></i> Reddit</a>
                </div>
            </div>

            <hr>
        </div>
    </section>

    <section id="unitrade-buy-token" class="section">
        <div class="container">
            <h1>Start trading <span>UniTrade</span> today</h1>
            <p>Here are a few exchanges you can currently buy UniTrade (TRADE).</p>
            <div class="row">
                <div class="col-12">
                    <a href="https://uniswap.info/token/0x6f87d756daf0503d08eb8993686c7fc01dc44fb1" target="_blank"><img src="images/uniswap-unitrade-exchange.jpg" alt="Trade on Uniswap (TRADE)" /></a>
                </div>

                <div class="col-lg-6 col-12">
                    <a href="https://idex.market/eth/trade" target="_blank">
                        <img src="./images/idex-exchange.jpg" alt="UniTrade (TRADE) on IDEX"/>
                    </a>
                </div>

                <div class="col-lg-6 col-12">
                    <a href="https://poloniex.com/exchange/USDT_TRADE" target="_blank">
                        <img src="./images/poloniex-exchange.jpg" alt="UniTrade (TRADE) on Poloniex"/>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container-fluid">
            <div class="row footer-wrapper">
                <div class="col-md-5">
                    <div class="logo-wrapper">
                        <img src="images/unitrade_logo_transparent.png" alt="UniTrade Logo" />
                    </div>

                    <div class="social-wrapper">
                        <ul class="social-links gray">
                            <li><a href="https://t.me/UniTradeApp" target="_blank"><i class="fab fa-telegram-plane"></i></a></li>
                            <li><a href="https://twitter.com/UniTradeApp" target="_blank"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="https://discord.gg/tvH5bMm" target="_blank"><i class="fab fa-discord"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-6">
                    <ul class="footer-menu">
                        <li><a href="#unitrade-platform">About</a></li>
                        <li><a href="#unitrade-benefits">Features</a></li>
                        <li><a href="#what-is-unitrade-token-for">Token</a></li>
                        <li><a href="#unitrade-roadmap">Roadmap</a></li>
                        <li><a href="#unitrade-faq">FAQ</a></li>
                    </ul>
                </div>

                <div class="col-md-1">
                    <a class="back-to-top" href="#hero"><i class="far fa-caret-square-up"></i></a>
                </div>
            </div>

            <p class="copyright">&copy; 2020 - unitrade.app</p>
        </div>
    </footer>

    <!-- <div class="footer-dots-wrapper"> -->
        <div class="bg-img-1 footer-dots" style="background-image: url('images/footer-dots.png')"></div> 
    <!-- </div> -->

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
      </script>
    <script src="js/main.js"></script>
</body>
</html>